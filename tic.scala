/**
  * Created by zezima on 20.03.17.
  */

object TicTacToe {

  var bord = Array(0,0,0,0,0,0,0,0,0,0,0)

  def showBord(): Unit ={
    for(i<-0 until 9){
      if(bord(i) == 0) print(i)
      if(bord(i) == 1) print("x")
      if(bord(i) == 2) print("o")

      if((i+1) % 3 != 0) print("|") else println()
    }
    println()
  }

  def getMove(user: Int): Unit ={

    var position = 1
    var move = 0
    do{
      showBord()
      println("Gracz " + user + ", które pole wybierasz? ")
      val scanner = new java.util.Scanner(System.in)
      val line = scanner.nextLine()
      position = Integer.parseInt(line)
      if(bord(position) != 0) println("Wypierz poprawne pole")
      else {
        bord(position) = user
        move = 1
      }
    }while(move == 0)
  }

  def checkWin(): Int ={

    if(bord(0) == 1 && bord(1) == 1 && bord(2) == 1) return 1
    if(bord(3) == 1 && bord(4) == 1 && bord(5) == 1) return 1
    if(bord(6) == 1 && bord(7) == 1 && bord(8) == 1) return 1

    if(bord(0) == 1 && bord(3) == 1 && bord(6) == 1) return 1
    if(bord(1) == 1 && bord(4) == 1 && bord(7) == 1) return 1
    if(bord(2) == 1 && bord(5) == 1 && bord(8) == 1) return 1

    if(bord(0) == 1 && bord(4) == 1 && bord(8) == 1) return 1
    if(bord(2) == 1 && bord(4) == 1 && bord(6) == 1) return 1

    if(bord(0) == 2 && bord(1) == 2 && bord(2) == 2) return 2
    if(bord(3) == 2 && bord(4) == 2 && bord(5) == 2) return 2
    if(bord(6) == 2 && bord(7) == 2 && bord(8) == 2) return 2

    if(bord(0) == 2 && bord(3) == 2 && bord(6) == 2) return 2
    if(bord(1) == 2 && bord(4) == 2 && bord(7) == 2) return 2
    if(bord(2) == 2 && bord(5) == 2 && bord(8) == 2) return 2

    if(bord(0) == 2 && bord(4) == 2 && bord(8) == 2) return 2
    if(bord(2) == 2 && bord(4) == 2 && bord(6) == 2) return 2

    return 0
  }



  def main(args: Array[String]): Unit = {

    var player = 1
    while(checkWin() == 0){

      getMove(player)
      if(checkWin() == 0) player = player + 1
      if(player == 3) player = 1
    }
    showBord()
    println("Wygrał gracz: " + player)
    println(" GRATULACJE! ")
  }
}

TicTacToe.main(Array())